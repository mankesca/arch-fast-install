const termcolors = require('termcolors');

function toVSCodeTerminalTheme(str) {
    let parsedColors = termcolors.xresources.import(str);
    let colorScheme = {
        'terminal.foreground': 'foreground',
        'terminal.background': 'background',
        'terminal.ansiBlack': '0',
        'terminal.ansiBrightBlack': '8',
        'terminal.ansiRed': '1',
        'terminal.ansiBrightRed': '9',
        'terminal.ansiGreen': '2',
        'terminal.ansiBrightGreen': '10',
        'terminal.ansiYellow': '3',
        'terminal.ansiBrightYellow': '11',
        'terminal.ansiBlue': '4',
        'terminal.ansiBrightBlue': '12',
        'terminal.ansiMagenta': '5',
        'terminal.ansiBrightMagenta': '13',
        'terminal.ansiCyan': '6',
        'terminal.ansiBrightCyan': '14',
        'terminal.ansiWhite': '7',
        'terminal.ansiBrightWhite': '15'
    };
    
    for (const key in colorScheme) {
        if (colorScheme.hasOwnProperty(key)) {
            colorScheme[key] = parsedColors[colorScheme[key]].toHex() || '';
        } 
    }

    console.log(JSON.stringify(colorScheme));
}

toVSCodeTerminalTheme(`
#define base00 #333333
#define base01 #393939
#define base02 #515151
#define base03 #747369
#define base04 #a09f93
#define base05 #d3d0c8
#define base06 #e8e6df
#define base07 #f2f0ec
#define base08 #f2777a
#define base09 #f99157
#define base0A #ffcc66
#define base0B #99cc99
#define base0C #66cccc
#define base0D #6699cc
#define base0E #cc99cc
#define base0F #d27b53

*foreground:   base05
*background:   base00
*cursorColor:  base05
*color0:       #000000
*color1:       base08
*color2:       base0B
*color3:       base0A
*color4:       base0D
*color5:       base0E
*color6:       base0C
*color7:       base05
*color8:       base03
*color9:       base08
*color10:      base0B
*color11:      base0A
*color12:      base0D
*color13:      base0E
*color14:      base0C
*color15:      base07
*color16:      base09
*color17:      base0F
*color18:      base01
*color19:      base02
*color20:      base04
*color21:      base06
`);
