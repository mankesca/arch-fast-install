!! Fontconfig (https://wiki.archlinux.org/index.php/font_configuration#Applications_without_fontconfig_support)

Xft.dpi:       192
Xft.antialias: true
Xft.hinting:   true
Xft.rgba:      rgb
Xft.autohint:  false
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault

!! URxvt (http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.1.pod)
!! Compile options (http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.7.pod)
!!      ./configure \
!!          --prefix=/usr \
!!          --enable-perl \
!!          --enable-frills \
!!          --enable-startup-notification \
!!          --enable-256-color \
!!          --enable-pixbuf \
!!          --enable-transparency \
!!          --enable-fading \
!!          --enable-pointer-blank \
!!          --enable-combining \
!!          --enable-font-styles \
!!          --enable-xft \
!!          --enable-unicode3 \
!!          --enable-mousewheel \
!!          --enable-slipwheeling \
!!          --enable-selectionscrolling \
!!          --enable-utmp \
!!          --enable-wtmp \
!!          --enable-lastlog \
!!          --disable-smart-resize \
!!          --disable-xim \
!!          --disable-iso14755 \
!!          --disable-keepscrolling \
!!          --disable-rxvt-scroll \
!!          --disable-next-scroll \
!!          --disable-xterm-scroll
!!      make; make install
!! Patches
!!      sgr-mouse-mode.patch (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=847984)
!!      background-img-use-color.patch (https://github.com/ranger/ranger/issues/1106)
!! AUR (https://aur.archlinux.org/packages/rxvt-unicode-cvs/)
!! Default perl moduled embeded (http://cvs.schmorp.de/rxvt-unicode/src/perl/?hideattic=1)
!! Default perl modules loaded (http://cvs.schmorp.de/rxvt-unicode/src/urxvt.pm?revision=1.257&view=markup#l733)



! True: enable the scrollbar [default]; option -sb. False: disable the scrollbar; option +sb.
URxvt*scrollBar: false



! Turn on/off ISO 14755 (default enabled).
!       (https://en.wikipedia.org/wiki/ISO/IEC_14755)
!       (http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.1.pod#ISO_14755_SUPPORT)
URxvt.iso14755: false

! Turn on/off ISO 14755 5.2 mode (default enabled).
URxvt.iso14755_52: false



! True: scroll to bottom when tty receives output; option -si. False: do not scroll to bottom when tty receives output; option +si.
URxvt*scrollTtyOutput: false

! True: scroll with scrollback buffer when tty receives new lines (i.e. try to show the same lines) and scrollTtyOutput is False; option -sw. False: do not scroll with scrollback buffer when tty receives new lines; option +sw.
URxvt*scrollWithBuffer: true

! True: scroll to bottom when a non-special key is pressed. Special keys are those which are intercepted by rxvt-unicode for special handling and are not passed onto the shell; option -sk. False: do not scroll to bottom when a non-special key is pressed; option +sk.
URxvt*scrollTtyKeypress: true

! Turn on/off secondary screen (default enabled). 
URxvt*secondaryScreen: true

! Turn on/off secondary screen scroll (default enabled). If this option is enabled, scrolls on the secondary screen will change the scrollback buffer and, when secondaryScreen is off, switching to/from the secondary screen will instead scroll the screen up.
URxvt*secondaryScroll: false



! Comma-separated list(s) of perl extension scripts (default: default) to use in this terminal instance; option -pe.
!       autocomplete-ALL-the-things -> (https://github.com/Vifon/autocomplete-ALL-the-things)
!       font-size -> (https://aur.archlinux.org/packages/urxvt-font-size-git/)
!       * keyboard-select -> (https://github.com/pkkolos/urxvt-scripts/blob/master/keyboard-select)
!       * matcher -> (http://cvs.schmorp.de/rxvt-unicode/src/perl/matcher?hideattic=1&view=markup)
!       vim-insert -> (https://github.com/seletskiy/urxvt-vim-insert)
!       vim-scrollback -> (https://github.com/ervandew/urxvt-vim-scrollback)
!       * vtwheel-vte -> (https://aur.archlinux.org/packages/urxvt-vtwheel-vte/)
URxvt*perl-ext-common: default,keyboard-select,matcher,vtwheel-vte

! keyboard-select
URxvt.keysym.M-Escape: perl:keyboard-select:activate
URxvt.keysym.M-s: perl:keyboard-select:search

! matcher
URxvt.url-launcher: chromium
URxvt.keysym.S-Delete: matcher:select
URxvt.keysym.C-Delete: matcher:last
URxvt.keysym.M-Delete: matcher:list











*background:                      #222D31
*foreground:                      #d8d8d8
*fading:                          8
*fadeColor:                       black
*cursorColor:                     #1ABB9B
*pointerColorBackground:          #2B2C2B
*pointerColorForeground:          #16A085

!! black dark/light
*color0:                          #222D31
*color8:                          #585858

!! red dark/light
*color1:                          #ab4642
*color9:                          #ab4642

!! green dark/light
*color2:                          #7E807E
*color10:                         #8D8F8D

!! yellow dark/light
*color3:                          #f7ca88
*color11:                         #f7ca88

!! blue dark/light
*color4:                          #7cafc2
*color12:                         #7cafc2

!! magenta dark/light
*color5:                          #ba8baf
*color13:                         #ba8baf

!! cyan dark/light
*color6:                          #1ABB9B
*color14:                         #1ABB9B

!! white dark/light
*color7:                          #d8d8d8
*color15:                         #f8f8f8

Xcursor.theme: xcursor-breeze
Xcursor.size:                     0

URxvt.font:                       9x15,xft:TerminessTTFNerdFontMono


URxvt.depth:                      32
URxvt.background:                 [100]#222D31
URxvt*scrollBar:                  false
URxvt*mouseWheelScrollPage:       false
URxvt*cursorBlink:                true
URxvt*background:                 black
URxvt*foreground:                 grey
URxvt*saveLines:                  5000

! for 'fake' transparency (without Compton) uncomment the following three lines
! URxvt*inheritPixmap:            true
! URxvt*transparent:              true
! URxvt*shading:                  138

! Normal copy-paste keybindings without perls
URxvt.iso14755:                   false
URxvt.keysym.Shift-Control-V:     eval:paste_clipboard
URxvt.keysym.Shift-Control-C:     eval:selection_to_clipboard
!Xterm escape codes, word by word movement
URxvt.keysym.Control-Left:        \033[1;5D
URxvt.keysym.Shift-Control-Left:  \033[1;6D
URxvt.keysym.Control-Right:       \033[1;5C
URxvt.keysym.Shift-Control-Right: \033[1;6C
URxvt.keysym.Control-Up:          \033[1;5A
URxvt.keysym.Shift-Control-Up:    \033[1;6A
URxvt.keysym.Control-Down:        \033[1;5B
URxvt.keysym.Shift-Control-Down:  \033[1;6B

