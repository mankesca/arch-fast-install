#!/usr/bin/env bash

# use extended pattern matching operators like *(pattern-list)
# https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html
shopt -s extglob

# use the default C locale for interpret ranges in bracket expressions
shopt -s globasciiranges

# remove non-matching globs
shopt -s nullglob

# inherite any trap on ERR by shell functions, command substitutions, or commands executed in a sub‐shell environment
# https://stackoverflow.com/a/29412038/9099846
set -o errtrace

app.GO() {
    APP_DEBUG=false

    APP_EXEC=()
    APP_EXEC_MAIN=true

    app.enable_trap "INT"
    app.enable_trap "ERR"

    if hash "app.args" &> /dev/null; then
        app.args "$@"
    fi

    if hash "app.init" &> /dev/null; then
        app.init
    fi

    if (( ${#APP_EXEC[@]} )); then
        local function

        for function in "${APP_EXEC[@]}"; do
            "$function"
        done
    else
        if hash "app.main" &> /dev/null; then
            app.main
        fi
    fi
}

app.exec() {
    local matching=false
    local function

    APP_EXEC_MAIN=false

    for function in "${APP_EXEC[@]}"; do
        if [[ "$*" == "$function" ]]; then
            matching=true && break
        fi
    done

    if ! $matching; then
        local name="$(sed -nr -e 's/(app.)?([[:alnum:]_]+)$/\2/p' <<< "$*")"
        name="${name^^}"

        if [[ -n "$name" ]]; then
            declare -g APP_EXEC_${name}=true
            APP_EXEC+=("$*")
        else
            logger.fatal "'${function}' is not a valid variable name"
        fi
    fi
}

app.declare_array() {
    declare -g -a "$1"
    local -n array="$1"
    shift

    array+=("$*")
}

app.enable_trap() {
    trap "${2:-"exit 1"}" "$1"
}

app.disable_trap() {
    trap - "$1"
}

# check if a value is present in an array
#
# @param (string) $1 the name of the array to query
# @param (string) $* the value to search
array.include() {
    local -n array="$1"
    shift

    for element in "${array[@]}"; do
        if [[ $element == "$*" ]]; then
            return 0
        fi
    done

    return 1
}

# append a string at the end of a line
#
# @param (string) $1 the matching line pattern
# @param (string) $2 the value to append
# @param (string) $3 the path of the file to query
file.append_string_to_endline() {
    sed -i "/$1/s/$/$2/" "$3"
}

# check if a file exist and is readable
#
# @param (string) $* the path of the file
file.is_readable() {
    if [[ ! -f "$*" || ! -r "$*" ]]; then
        logger.debug "File '$*' not readable"
        return 1
    fi

    return 0
}

# check if a file exist and is writable
#
# @param (string) $* the path of the file
file.is_writable() {
    if [[ ! -f "$*" || ! -w "$*" ]]; then
        logger.debug "File '$*' not writable"
        return 1
    fi

    return 0
}

file.default() {
    if [[ ! -f "${*}.default" ]]; then
        cp "$*" "${*}.default"
    fi
}

file.reset() {
    file.default "$*"
    cp "${*}.default" "$*"
}

# print the given message with a tag and a color that reflects its purpose
#
# @param (string) $* the message to print
logger.info() {
    echo -e "\e[36m[INFO]\e[0m \e[96m${*}\e[0m" # blue
}

logger.success() {
    echo -e "\e[32m[SUCCESS]\e[0m \e[92m${*}\e[0m" # green
}

logger.warn() {
    echo -e "\e[33m[WARN]\e[0m \e[93m${*}\e[0m" # yellow
}

logger.error() {
    echo -e "\e[31m[ERROR]\e[0m \e[91m${*}\e[0m" # red
}

logger.fatal() {
    echo -e "\e[31m[FATAL] (${FUNCNAME[1]})\e[0m \e[91m${*}\e[0m" # red
    exit 1
}

logger.debug() {
    if $APP_DEBUG; then
        echo -e "\e[31m[DEBUG] (${FUNCNAME[1]})\e[0m \e[91m${*}\e[0m" # red
    fi
}

# bash read builtin wrapper
#
# @param (string) $1 the name of the variable to populate
# @param (string) $2 the name of the array to refer

<< @example
    declare -A local __READ_OPTS__=(
        [prompt]="Type:"
        [again]="Again:" # or true to check the typed value without displaying a message

        [inline]=true # by default
        [secure]=false # by default
        [test]="read.is_present" # by default

        # if the placeholder %default% is found in prompt/again/error message
        # replace this by the default value
        [default]="my_default_value"

        [error.prompt]="Invalid"
        [error.again]="Different"
    ) && read.new "my_variable_name" __READ_OPTS__
@example

read.new() {
    local varname="$1"

    # access to the array by reference
    if [[ -n "$2" ]]; then
        local -n opts="$2"
    else
        declare -A opts
    fi

    if [[ -z "$varname" ]]; then
        logger.fatal "The name of the target variable is required"
    fi

    local prompt="${opts[prompt]}"
    local default="${opts[default]}"
    local inline="${opts[inline]}"
    local secure="${opts[secure]}"

    if [[ -n "$prompt" ]]; then
        if [[ -n "$default" ]]; then
            prompt="${prompt//%default%/$default}"
        fi

        read.@echo "$prompt"
    fi

    read.@read

    if [[ -n "$default" && -z "${!varname}" ]]; then
        declare -g "$varname"="$default"
        return 0
    fi

    local test="${opts[test]}"
    local errormsg="${opts[error.prompt]}"

    if [[ -n "$errormsg" ]]; then
        test="${test:-"read.is_present"}"
    fi

    if [[ -n "$test" ]]; then
        if ! hash "$test" &> /dev/null; then
            logger.fatal "The function $test is not defined"
        fi

        local return_code
        while "$test" "${!varname}"; (( return_code=$? )); do
            if (( return_code > 1 )); then
                return $return_code
            fi

            if [[ -n "$errormsg" ]]; then
                if [[ -n "$default" ]]; then
                    errormsg="${errormsg//%default%/$default}"
                fi

                read.@echo "$errormsg"
            fi

            read.@read
        done
    fi

    if [[ -n "${opts[again]}" && "${opts[again]}" != false ]]; then
        local again="${opts[again]}"
        local errormsg="${opts[error.again]}"

        if [[ -n "$again" && "${opts[again]}" != true ]]; then
            if [[ -n "$default" ]]; then
                again="${again//%default%/$default}"
            fi

            read.@echo "$again"
        fi

        read.@read "verify_varname"

        if [[ "${!varname}" != "$verify_varname" ]]; then
            if [[ -n "$errormsg" ]]; then
                echo -e "${errormsg}"
            fi

            read.new $@
        fi
    fi
}

# @private read.new
read.@read() {
    local inline="${inline:-true}"
    local secure="${secure:-false}"

    read "-r$("$secure" && echo "s")" "${*:-$varname}"

    if "$secure" && "$inline"; then
        printf "\n"
    fi
}

# @private read.new
read.@echo() {
    local inline="${inline:-true}"
    "$inline" && printf "$* " || printf "$*\n"
}

# read only if the supplied variable isn't defined
#
# @same read.new
read.new_if_undefined() {
    if [[ ! -v "$1" ]]; then
        read.new $@
    fi
}

# bash read builtin wrapper configured to read and confirm a password
#
# @param (string) $1 the name of the variable to populate
# @param (string) $2 the label of the password
read.new_password() {
    declare -A local __READ_OPTS__=(
        [prompt]="New "${2:+"$2 "}"password:"
        [again]="Retype "${2:-"new"}" password:"
        [error.prompt]="No password supplied\nNew "${2:+"$2 "}"password:"
        [error.again]="Sorry, passwords do not match"
        [secure]=true
    ) && read.new "$1" __READ_OPTS__
}

# read password only if the supplied variable isn't defined
#
# @same read.new_password
read.new_password_if_undefined() {
    if [[ ! -v "$1" ]]; then
        read.new_password $@
    fi
}

# bash read builtin wrapper configured to read a secret value
#
# @param (string) $1 the name of the variable to populate
# @param (string) $2 the label of the secret
read.new_secret() {
    declare -A local __READ_OPTS__=(
        [prompt]="Type "${2:+"$2 "}"password:"
        [error.prompt]="No password supplied\nType "${2:+"$2 "}"password:"
        [secure]=true
    ) && read.new "$1" __READ_OPTS__
}

# read password only if the supplied variable isn't defined
#
# @same read.new_secret
read.new_secret_if_undefined() {
    if [[ ! -v "$1" ]]; then
        read.new_secret $@
    fi
}

# returns 0 if the given string is present, 1 otherwise
#
# @param (string) $*
read.is_present() {
    [[ -n "$*" ]] && return 0 || return 1
}

# returns 0 if the given string is present, 99 otherwise
#
# @param (string) $*
read.exit_if_empty() {
    [[ -n "$(string.strip "$*")" ]] && return 0 || return 99
}

# checks if a substring is present in a string
#
# @param (string) $1 the string to inspect
# @param (string) $2 the string to search
string.include() {
    *"$2"* == "$1" && return 0 || return 1
}

# checks if a string is a boolean
#
# @param (string) $* the string to inspect
string.is_boolean() {
    [[ "$*" == "true" || "$*" == "false" ]] && return 0 || return 1
}

# remove all leading and trailing whitespaces of a string
# https://stackoverflow.com/a/3352015/9099846
#
# @param (string) $* the string to inspect
string.strip() {
    local string="$*"

    # remove leading whitespace characters
    string="${string#"${string%%[![:space:]]*}"}"

    # remove trailing whitespace characters
    string="${string%"${string##*[![:space:]]}"}"

    printf "$string"
}
