#!/usr/bin/env bash

create_user() {
    local userstring="$*"
    [[ -z "$userstring" ]] && return 1

    local user=(${userstring//,/ })
    local identity=(${user[0]//:/ })
    local username="${identity[0]}"

    if [[ ! "$username" =~ [[:lower:]] ]]; then
        logger.error "${username:-"empty username"} is not valid" && return 1
    fi

    if string.is_boolean "${user[1]}"; then
        user[2]="${user[1]}"
        user[1]=""
    fi

    local password="${identity[1]}"
    local shell="/bin/${user[1]:-"bash"}"
    local sudo="${user[2]:-true}"

    logger.info "Add $username user"
    _chroot useradd -m -g users -G wheel -s "$shell" "$username" || return 1

    if [[ -z "$password" ]]; then
        read.new_password "password" "$username"
    fi

    echo "${username}:${password}" | chroot /mnt /usr/sbin/chpasswd

    if "$sudo"; then
        logger.info "Add $username as sudoer"
        echo "$username ALL=(ALL) ALL" > "/mnt/etc/sudoers.d/$username"
    fi
}

interactive_create_user() {
    local tmpuser
    local prompt="Add new user (<username:format=[a-z]>:<password:optional>,<shell:default=/bin/bash>,<root:default=true>)\n(press enter for skip):"

    local i=0; while true; do (( i+=1 ))
        if (( i == 2 )); then
            prompt="Add another user:"
        fi

        declare -A local READ_OPTS=(
            [prompt]="$prompt"
            [test]="read.exit_if_empty"
        )

        if ! read.new "tmpuser" READ_OPTS; then
            break
        fi

        create_user "$tmpuser"
    done
}
