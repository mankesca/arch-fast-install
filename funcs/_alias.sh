#!/usr/bin/env bash

# install a package with makepkg command
#
# @param (string) $1 the name of the package to install
# @param (string) $2 the git url of the PKGBUILD file
_makepkg() {
    local TOOL_NAME="$1"
    local GIT_URL="$2"
    local TMP_DIR="/tmp/.${TOOL_NAME}_install"

    if ! pacman -Q "$TOOL_NAME" &> /dev/null; then
        pacman -Sy --needed --noconfirm \
            base-devel \
            git

        logger.info "Install ${TOOL_NAME} package"
        git clone "$GIT_URL" "$TMP_DIR" || exit 1
        chown -R nobody "$TMP_DIR"

        cd "$TMP_DIR"
        sudo -u nobody makepkg --syncdeps --rmdeps
        pacman -U --needed --noconfirm ./*.pkg.tar.xz

        cd - &> /dev/null
        rm -rf "$TMP_DIR"
    fi
}

# wrapper of arch-chroot command with /mnt path
#
# @param (string) $@ the command and its arguments
_chroot() {
    local _TOOL
    $MANJARO && _TOOL="manjaro-chroot" || _TOOL="arch-chroot"

    $_TOOL /mnt "$@"
}

# wrapper of genfstab command
#
# @param (string) $@ arguments
_fstab() {
    local _TOOL
    $MANJARO && _TOOL="fstabgen" || _TOOL="genfstab"

    $_TOOL "$@"
}
