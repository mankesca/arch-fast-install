#!/usr/bin/env bash

app._declare_font() {
    if app._test_font "$*"; then
        FONT="latarcyrheb-sun32"
    fi
}

app._declare_hostname() {
    # if app._test_hostname "$*"; then
        NEWHOSTNAME="$*"
    # fi
}

app._declare_keymap() {
    # if app._test_keymap "$*"; then
        KEYMAP="$*"
    # fi
}

app._declare_timezone() {
    # if app._test_timezone "$*"; then
        TIMEZONE="$*"
    # fi
}

app._declare_user() {
    local userstring="$*"
    local user=(${userstring//,/ })
    local identity=(${user[0]//:/ })

    if [[ "${identity[0]}" == "root" ]]; then
        if [[ -z "${identity[1]}" ]]; then
            logger.fatal "Empty root password is not valid"
        fi

        if [[ -n "${user[1]}" ]]; then
            logger.warn "Please change the root user's shell manually"
        fi

        ROOT_PASS="${identity[1]}"
        return 0
    fi


    app.declare_array "USERS" "$1"
}

app._declare_part_option() {
    local prefixes=("EFI" "LVM" "ROOT" "SWAP")
    local prefix="${2:2:-(( ${#1} + 1 ))}"

    prefix="${prefix^^}"

    if [[ "$1" == "LABEL" ]]; then
        prefixes+=("DISK")
    fi

    if array.include prefixes "$prefix"; then
        declare -g "${prefix}_${1}"="$3"
    else
        logger.fatal "Unrecognized option '$2'"
    fi
}

app._test_font() {
    local fontname
    local fonts=()
    local target

    if [[ -n "$*" ]]; then
        for fontname in "/usr/share/kbd/consolefonts/"*"$*"*"."*gz; do
            if file.is_readable "$fontname"; then
                if [[ "$fontname"  == "/usr/share/kbd/consolefonts/${*}."* ]]; then
                    return 0
                fi

                fonts+=("$fontname")
            fi
        done
    fi

    local target
    local error

    if [[ -n "$fonts" ]]; then
        target=${fonts[@]}
        error="Ambiguous font name, please choose one from this list:"
    else
        target="/usr/share/kbd/consolefonts/"*gz
        error="Invalid font name, please choose one from this list:"
    fi

    fonts=$(for fontname in $target; do
        if file.is_readable "$fontname"; then
            fontname="${fontname##*/}"
            fontname="${fontname%%.*}"
            echo "$fontname"
        fi
    done | column -c $(tput cols))

    logger.error "$error\n$fonts"
    exit 1
}

# TODO: app._test_(timezone|locales|keymap)
