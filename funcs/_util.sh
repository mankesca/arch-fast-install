#!/usr/bin/env bash

default_swap_size() {
    # for a working hibernation (https://itsfoss.com/swap-size/), ram + 25% for simplicity
    echo "$(($(free --giga | awk '/^Mem:/{print $2}') * 125 / 100))G"
}

grep_dpath() {
    sed -nr -e '1,/Device/d; s/(\/[[:alnum:]\/-]+).*'"${1}"'/\1/p' <(fdisk -l "$DISK_DPATH")
}

grep_mpath() {
    sed -nr -e 's/(\/[[:alnum:]\/-]+)\s+'"${1}"'/\1/p' <(lsblk -lp -o NAME,FSTYPE)
}

luks_format() {
    # ask passphrase, use the same for root and swap partition
    # see Note: https://wiki.archlinux.org/index.php/Dm-crypt/System_configuration#crypttab

    read.new_password_if_undefined "LUKS_PASS" "luks"
    echo -n "$LUKS_PASS" | cryptsetup luksFormat "$1" -
}

luks_open() {
    read.new_secret_if_undefined "LUKS_PASS" "luks"
    echo -n "$LUKS_PASS" | cryptsetup luksOpen "$1" "$2" -
}

luks_close() {
    if is_encrypted "$*"; then
        cryptsetup luksClose "$*"
    fi
}

is_connected() {
    if [[ -z "$(curl -Is https://archlinux.org | head -1)" ]]; then
        return 1
    fi
}

is_partsize() {
    if [[ "$*" =~ ^[+-]?[[:digit:]]+[KMGTP]$ ]]; then
        return 0
    fi

    return 1
}

is_encrypted() {
    # need /dev/* path
    # blkid -o "export" "$*" | grep -q "TYPE=crypto_LUKS" && return 0 || return 1

    # need /dev/mapper/* path
    cryptsetup status "$*" &> /dev/null
    return $?
}

have_swap_without_lvm() {
    if "$SWAP" && ! "$LVM"; then
        return 0
    fi

    return 1
}
